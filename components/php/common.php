<?php
//categories
$News_category[0] = "All";
$News_category[1] = "Business, Finance & Economics";
$News_category[2] = "Computers, Science & Technology";
$News_category[3] = "Entertainment, Art & Culture";
$News_category[4] = "General News & Current Affairs";
$News_category[5] = "Health & Medicine";
$News_category[6] = "Sport & Leisure";
$News_category[7] = "Trade & Professional";
$News_category_values = range(0, 7);

// Iran_province
$Iran_province[1] = "آذربایجان شرقی";
$Iran_province[2] = "آذربایجان غربی";
$Iran_province[3] = "اردبیل";
$Iran_province[4] = "اصفهان";
$Iran_province[5] = "البرز";
$Iran_province[6] = "ایلام";
$Iran_province[7] = "بوشهر";
$Iran_province[8] = "تهران";
$Iran_province[9] = "چهارمحال و بختیاری";
$Iran_province[10] = "خراسان جنوبی";
$Iran_province[11] = "خراسان رضوی";
$Iran_province[12] = "خراسان شمالی";
$Iran_province[13] = "خوزستان";
$Iran_province[14] = "زنجان";
$Iran_province[15] = "سمنان";
$Iran_province[16] = "سیستان و بلوچستان";
$Iran_province[17] = "فارس";
$Iran_province[18] = "قزوین";
$Iran_province[19] = "قم";
$Iran_province[20] = "کردستان";
$Iran_province[21] = "کرمان";
$Iran_province[22] = "کرمانشاه";
$Iran_province[23] = "کهکیلویه و بویراحمد";
$Iran_province[24] = "گلستان";
$Iran_province[25] = "گیلان";
$Iran_province[26] = "لرستان";
$Iran_province[27] = "مازندران";
$Iran_province[28] = "مرکزی";
$Iran_province[29] = "هرمزگان";
$Iran_province[30] = "همدان";
$Iran_province[31] = "یزد";
$Iran_province[32] = "خارج کشور";
$Iran_province_values = range(1, 32);
