<?php

//DB Credentials
$servername = "127.0.0.1";
$username = "root";
$password = "123456";
$dbName = "php_learn";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbName);

// Check connection
if (mysqli_connect_errno())
{
    die("Connection failed: ". mysqli_connect_error());
}

$conn->query("SET NAMES 'utf8'");
$conn->query("SET CHARACTER SET 'utf8'");
$conn->query("SET character_set_connection = 'utf8'");

//echo "Connected successfully";
?>