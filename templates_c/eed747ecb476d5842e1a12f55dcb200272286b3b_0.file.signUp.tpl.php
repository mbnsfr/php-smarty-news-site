<?php /* Smarty version 3.1.27, created on 2021-01-13 07:11:01
         compiled from "/var/www/phplearn.local/templates/signUp.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:14370189655ffe9d05a1e961_65725601%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eed747ecb476d5842e1a12f55dcb200272286b3b' => 
    array (
      0 => '/var/www/phplearn.local/templates/signUp.tpl',
      1 => 1610521859,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14370189655ffe9d05a1e961_65725601',
  'variables' => 
  array (
    'Iran_province' => 0,
    'key' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5ffe9d05a2c849_51969275',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5ffe9d05a2c849_51969275')) {
function content_5ffe9d05a2c849_51969275 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '14370189655ffe9d05a1e961_65725601';
?>
<!doctype html>
<html lang="fa">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="icon" href="/components/image/favicon.ico" type="image/png" />
    <link rel="stylesheet" type="text/css" href="components/css/Master.css" />
    <title>ثبت نام</title>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        html,
        body {
            height: 100%;
        }

        body {
            display: flex;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }

        .form-signin .checkbox {
            font-weight: 400;
        }

        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }

        .form-signin .form-control:focus {
            z-index: 2;
        }

        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
</head>

<body class="text-center">
    <main class="form-signin">
        <form>
            <img class="mb-4" src="components/image/header.png" alt="" width="240" height="100">
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">@</span>
                <input type="text" class="form-control" placeholder="Username" aria-label="Username"
                    aria-describedby="basic-addon1" required="">
            </div>
            <div class="input-group mb-3">
                <label class="input-group-text" for="inputGroupSelect01">City</label>
                <select class="form-select" id="inputGroupSelect01" required>
                    <option value="" selected>Choose...</option>
                    <?php
$_from = $_smarty_tpl->tpl_vars['Iran_province']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                        <option value=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</option>
                    <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                </select>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">Email</span>
                <input type="text" id="email" class="form-control" placeholder="Email address" aria-label="email"
                    aria-describedby="basic-addon1" required="">
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">Password</span>
                <input type="text" id="password" class="form-control" placeholder="Password" aria-label="password"
                    aria-describedby="basic-addon1" required="">
            </div>
            <div class="mb-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                        value="option1" required="">
                    <label class="form-check-label" for="inlineRadio1">female</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                        value="option2" required="">
                    <label class="form-check-label" for="inlineRadio2">male</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3"
                        value="option3" required="">
                    <label class="form-check-label" for="inlineRadio3">others</label>
                </div>
            </div>
            <button class="w-100 btn btn-lg btn-info" type="submit">Save</button>
        </form>
    </main>
</body>
<?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"><?php echo '</script'; ?>
><?php }
}
?>