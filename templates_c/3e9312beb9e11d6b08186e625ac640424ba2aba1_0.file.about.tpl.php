<?php /* Smarty version 3.1.27, created on 2021-01-08 11:11:38
         compiled from "/var/www/phplearn.local/templates/about.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:13430981655ff83dea573270_80622991%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e9312beb9e11d6b08186e625ac640424ba2aba1' => 
    array (
      0 => '/var/www/phplearn.local/templates/about.tpl',
      1 => 1610104292,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13430981655ff83dea573270_80622991',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5ff83dea580131_10881407',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5ff83dea580131_10881407')) {
function content_5ff83dea580131_10881407 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '13430981655ff83dea573270_80622991';
?>
<!doctype html>
<html lang="fa">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="components/css/Master.css" />
    <link rel="icon" href="/components/image/favicon.ico" type="image/png" />
    <title>درباره ما</title>
</head>

<body>
    <?php echo $_smarty_tpl->getSubTemplate ("templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"Info"), 0);
?>

    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10" style="text-align: right;">
                <strong>درباره ‌ی ما </strong>
                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها
                    و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                    کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و
                    آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیشتری را برای طراحان رایانه
                    ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی ایجاد کرد. در این صورت می توان امید داشت که
                    تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی
                    دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                </p>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"Info"), 0);
?>

</body>

</html><?php }
}
?>