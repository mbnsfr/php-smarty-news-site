<?php /* Smarty version 3.1.27, created on 2021-01-12 16:45:36
         compiled from "/var/www/phplearn.local/templates/news.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:13905453365ffdd230b50512_04218596%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0416de93de061f61df8b0beb1416c9ac8e74bfae' => 
    array (
      0 => '/var/www/phplearn.local/templates/news.tpl',
      1 => 1610469934,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13905453365ffdd230b50512_04218596',
  'variables' => 
  array (
    'category' => 0,
    'key' => 0,
    'type' => 0,
    'item' => 0,
    'news' => 0,
    'empty' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5ffdd230b6a0d0_17510013',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5ffdd230b6a0d0_17510013')) {
function content_5ffdd230b6a0d0_17510013 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '13905453365ffdd230b50512_04218596';
?>
<!doctype html>
<html lang="fa">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="icon" href="/components/image/favicon.ico" type="image/png" />
    <link rel="stylesheet" type="text/css" href="components/css/Master.css" />
    <title>اخبار</title>
</head>

<body>
    <?php echo $_smarty_tpl->getSubTemplate ("templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"Info"), 0);
?>

    <div class="container" style="margin-top: 110px!important;">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">NEWS</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group">
                    <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        categry
                    </button>
                    <ul class="dropdown-menu">
                        <?php
$_from = $_smarty_tpl->tpl_vars['category']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
$_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                            <?php if ($_smarty_tpl->tpl_vars['key']->value == $_smarty_tpl->tpl_vars['type']->value) {?>
                                <li><a class="dropdown-item active" href="news.php?category=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</a></li>
                            <?php } else { ?>
                                <li><a class="dropdown-item" href="news.php?category=<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</a></li>
                            <?php }?>
                        <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
$_from = $_smarty_tpl->tpl_vars['news']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="card mb-3">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value[6];?>
" class="card-img-top" alt="news">
                        <div class="card-img-overlay">
                            <a href="news.php" class="card-text btn btn-primary btn-sm" tabindex="-1" role="button">
                                more
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $_smarty_tpl->tpl_vars['item']->value[1];?>
</h5>
                            <p class="card-text"><?php echo $_smarty_tpl->tpl_vars['item']->value[2];?>
</p>
                            <p class="card-text">
                                <small class="text-muted"><?php echo $_smarty_tpl->tpl_vars['item']->value[5];?>
</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
            <?php if ($_smarty_tpl->tpl_vars['empty']->value == true) {?>
                <div class="col-md-12">
                    <div class="alert alert-warning" role="alert">
                        no result found , please visit <a href="news.php" class="alert-link">main news</a> page. thanks.
                    </div>
                </div>
            <?php }?>
        </div>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"Info"), 0);
?>

</body>

</html><?php }
}
?>