<?php /* Smarty version 3.1.27, created on 2021-01-13 06:59:18
         compiled from "/var/www/phplearn.local/templates/signIn.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:18927848885ffe9a46a9e6a6_81543120%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2a64ac164b5402e52e26e3ec55410341303d058a' => 
    array (
      0 => '/var/www/phplearn.local/templates/signIn.tpl',
      1 => 1610521157,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18927848885ffe9a46a9e6a6_81543120',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5ffe9a46aa3357_16499465',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5ffe9a46aa3357_16499465')) {
function content_5ffe9a46aa3357_16499465 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '18927848885ffe9a46a9e6a6_81543120';
?>
<!doctype html>
<html lang="fa">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="icon" href="/components/image/favicon.ico" type="image/png" />
    <link rel="stylesheet" type="text/css" href="components/css/Master.css" />
    <title> ورود</title>
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        html,
        body {
            height: 100%;
        }

        body {
            display: flex;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #f5f5f5;
        }

        .form-signin {
            width: 100%;
            max-width: 330px;
            padding: 15px;
            margin: auto;
        }

        .form-signin .checkbox {
            font-weight: 400;
        }

        .form-signin .form-control {
            position: relative;
            box-sizing: border-box;
            height: auto;
            padding: 10px;
            font-size: 16px;
        }

        .form-signin .form-control:focus {
            z-index: 2;
        }

        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
</head>

<body class="text-center">
    <main class="form-signin">
        <form>
            <img class="mb-4" src="components/image/header.png" alt="" width="240" height="100">
            <label for="inputEmail" class="visually-hidden">Email address</label>
            <input type="email" id="inputEmail" class="form-control mb-1" placeholder="Email address" required=""
                autofocus="">
            <label for="inputPassword" class="visually-hidden">Password</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">
            <div class="checkbox mb-2 text-start">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="w-100 btn btn-lg btn-primary mb-1" type="submit">Sign in</button>
            <a href="signUp.php" class="w-100 btn btn-lg btn-info" tabindex="-1" role="button">
                Sign up
            </a>
        </form>
    </main>
</body>
<?php echo '<script'; ?>
 src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"><?php echo '</script'; ?>
><?php }
}
?>