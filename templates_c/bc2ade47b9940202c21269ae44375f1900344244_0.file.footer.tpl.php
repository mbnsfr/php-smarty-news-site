<?php /* Smarty version 3.1.27, created on 2021-01-12 16:46:24
         compiled from "/var/www/phplearn.local/templates/footer.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:21132865745ffdd260a4edf8_98132871%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bc2ade47b9940202c21269ae44375f1900344244' => 
    array (
      0 => '/var/www/phplearn.local/templates/footer.tpl',
      1 => 1610469982,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '21132865745ffdd260a4edf8_98132871',
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5ffdd260a51557_32093310',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5ffdd260a51557_32093310')) {
function content_5ffdd260a51557_32093310 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '21132865745ffdd260a4edf8_98132871';
?>
<footer class="bd-footer mt-auto py-3 p-3 p-md-5 bg-light text-center text-sm-start">
    <div class="container ar">
        <div class="row">
            <div class="col-md-4">
                <p>
                    شماره تماس : ۲۲۴۴۲۸۳۱
                    <img src="components/bootstrap-icons/telephone-fill.svg" alt="">
                </p>
                <p>
                    ادرس : ازگل-میدان ازگل-ابتدای کامران
                    <img src="components/bootstrap-icons/geo-alt-fill.svg" alt="">
                </p>
            </div>
            <div class="col-md-4">
                <a href="404.php">404</a>
                <br />
                <a href="about.php">about</a>
                <br />
                <a href="index.php">main</a>
                <br />
                <a href="news.php">news</a>
            </div>
            <div class="col-md-4 text-center">
                <img src="components/image/image.png" class="img-fluid" alt="logo">
            </div>
        </div>
    </div>
</footer><?php }
}
?>