<?php /* Smarty version 3.1.27, created on 2021-01-13 05:58:22
         compiled from "/var/www/phplearn.local/templates/index.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:468491845ffe8bfe0950f3_49995196%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ecbe43b9a32eee750d36317b1f68a072a2b38a29' => 
    array (
      0 => '/var/www/phplearn.local/templates/index.tpl',
      1 => 1610517498,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '468491845ffe8bfe0950f3_49995196',
  'variables' => 
  array (
    'carousel' => 0,
    'item' => 0,
    'news' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_5ffe8bfe181e97_98257492',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_5ffe8bfe181e97_98257492')) {
function content_5ffe8bfe181e97_98257492 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '468491845ffe8bfe0950f3_49995196';
?>
<!doctype html>
<html lang="fa">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="icon" href="/components/image/favicon.ico" type="image/png" />
    <link rel="stylesheet" type="text/css" href="components/css/Master.css" />
    <title>صفحه اصلی</title>
</head>

<body>
    <?php echo $_smarty_tpl->getSubTemplate ("templates/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"Info"), 0);
?>

    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel"  style="margin-top: 110px!important;">
        <ol class="carousel-indicators">
            <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
            <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <?php
$_from = $_smarty_tpl->tpl_vars['carousel']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
$_smarty_tpl->tpl_vars['__foreach_foo'] = new Smarty_Variable(array('iteration' => 0));
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$_smarty_tpl->tpl_vars['__foreach_foo']->value['iteration']++;
$_smarty_tpl->tpl_vars['__foreach_foo']->value['first'] = $_smarty_tpl->tpl_vars['__foreach_foo']->value['iteration'] == 1;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                <?php if ((isset($_smarty_tpl->tpl_vars['__foreach_foo']->value['first']) ? $_smarty_tpl->tpl_vars['__foreach_foo']->value['first'] : null)) {?>
                    <div class="carousel-item active">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value[6];?>
" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block text-center">
                            <h4><?php echo $_smarty_tpl->tpl_vars['item']->value[1];?>
</h4>
                            <p><?php echo $_smarty_tpl->tpl_vars['item']->value[2];?>
</p>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="carousel-item">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value[6];?>
" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block text-center">
                            <h4><?php echo $_smarty_tpl->tpl_vars['item']->value[1];?>
</h4>
                            <p><?php echo $_smarty_tpl->tpl_vars['item']->value[2];?>
</p>
                        </div>
                    </div>
                <?php }?>
            <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </a>
    </div>
    <br />
    <div class="container">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">NEWS</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="news.php" class="btn btn-outline-secondary btn-sm" tabindex="-1" role="button"
                    aria-disabled="true">more</a>
            </div>
        </div>
        <div class="row">
            <?php
$_from = $_smarty_tpl->tpl_vars['news']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['item']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
$foreach_item_Sav = $_smarty_tpl->tpl_vars['item'];
?>
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="card mb-3">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['item']->value[6];?>
" class="card-img-top" alt="news">
                        <div class="card-img-overlay">
                            <a href="news.php" class="card-text btn btn-primary btn-sm" tabindex="-1" role="button">
                                more
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $_smarty_tpl->tpl_vars['item']->value[1];?>
</h5>
                            <p class="card-text"><?php echo $_smarty_tpl->tpl_vars['item']->value[2];?>
</p>
                            <p class="card-text">
                                <small class="text-muted"><?php echo $_smarty_tpl->tpl_vars['item']->value[5];?>
</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            <?php
$_smarty_tpl->tpl_vars['item'] = $foreach_item_Sav;
}
?>
        </div>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("templates/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"Info"), 0);
?>

</body>

</html><?php }
}
?>