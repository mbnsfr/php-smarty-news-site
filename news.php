<?php
session_start();
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 0);

//Includes necessary files
include('vendor/smarty/smarty/libs/Smarty.class.php');
include('components/php/db_connect.php');
include('components/php/common.php');

// create object
$smarty = new Smarty;

$category_query = "";
$category_type = "0";
$empty = false;

if ($_GET['category']) {
    $category_type = $_GET['category'];
    $category = intval($_GET['category']);
    $category_query = "where News_Type=$category";
}

$q = "SELECT * FROM news $category_query";
$q_result = mysqli_query($conn, $q);
$news = mysqli_fetch_all($q_result);

if (count($news) == 0) {
    $empty = true;
}

// assign it
$smarty->assign('news', $news);
$smarty->assign('empty', $empty);
$smarty->assign('type', $category_type);
$smarty->assign('category', $News_category);

// display it
$smarty->display('templates/news.tpl');
