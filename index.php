<?php
session_start();
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 0);

//Includes necessary files
include('vendor/smarty/smarty/libs/Smarty.class.php');
include('components/php/db_connect.php');

// create object
$smarty = new Smarty;

$q = "SELECT * FROM news";
$q_result = mysqli_query($conn, $q);
$news = mysqli_fetch_all($q_result);

$query = "SELECT * FROM news LIMIT 3";
$query_result = mysqli_query($conn, $query);
$carousel = mysqli_fetch_all($query_result);

// assign it
$smarty->assign('news', $news);
$smarty->assign('carousel', $carousel);

// display it
$smarty->display('templates/index.tpl');
