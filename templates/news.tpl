<!doctype html>
<html lang="fa">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="icon" href="/components/image/favicon.ico" type="image/png" />
    <link rel="stylesheet" type="text/css" href="components/css/Master.css" />
    <title>اخبار</title>
</head>

<body>
    {include file="templates/header.tpl" title="Info"}
    <div class="container" style="margin-top: 110px!important;">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">NEWS</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <div class="btn-group">
                    <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        categry
                    </button>
                    <ul class="dropdown-menu">
                        {foreach from=$category item=item key=key}
                            {if $key == $type}
                                <li><a class="dropdown-item active" href="news.php?category={$key}">{$item}</a></li>
                            {else}
                                <li><a class="dropdown-item" href="news.php?category={$key}">{$item}</a></li>
                            {/if}
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            {foreach from=$news item=item}
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="card mb-3">
                        <img src="{$item[6]}" class="card-img-top" alt="news">
                        <div class="card-img-overlay">
                            <a href="news.php" class="card-text btn btn-primary btn-sm" tabindex="-1" role="button">
                                more
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{$item[1]}</h5>
                            <p class="card-text">{$item[2]}</p>
                            <p class="card-text">
                                <small class="text-muted">{$item[5]}</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            {/foreach}
            {if $empty == true}
                <div class="col-md-12">
                    <div class="alert alert-warning" role="alert">
                        no result found , please visit <a href="news.php" class="alert-link">main news</a> page. thanks.
                    </div>
                </div>
            {/if}
        </div>
    </div>
    {include file="templates/footer.tpl" title="Info"}
</body>

</html>