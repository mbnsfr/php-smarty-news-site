<!doctype html>
<html lang="fa">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="components/css/Master.css" />
    <link rel="icon" href="/components/image/favicon.ico" type="image/png" />
    <title>صفحه مورد نظر یافت نشد</title>
</head>

<body>
    <img src="components/image/404.jpg" class="d-block w-100" alt="home">
</body>

</html>