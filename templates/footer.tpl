<footer class="bd-footer mt-auto py-3 p-3 p-md-5 bg-light text-center text-sm-start">
    <div class="container ar">
        <div class="row">
            <div class="col-md-4">
                <p>
                    شماره تماس : ۲۲۴۴۲۸۳۱
                    <img src="components/bootstrap-icons/telephone-fill.svg" alt="">
                </p>
                <p>
                    ادرس : ازگل-میدان ازگل-ابتدای کامران
                    <img src="components/bootstrap-icons/geo-alt-fill.svg" alt="">
                </p>
            </div>
            <div class="col-md-4">
                <a href="404.php">404</a>
                <br />
                <a href="about.php">about</a>
                <br />
                <a href="index.php">main</a>
                <br />
                <a href="news.php">news</a>
            </div>
            <div class="col-md-4 text-center">
                <img src="components/image/image.png" class="img-fluid" alt="logo">
            </div>
        </div>
    </div>
</footer>