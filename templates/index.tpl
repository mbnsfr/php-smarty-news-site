<!doctype html>
<html lang="fa">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="icon" href="/components/image/favicon.ico" type="image/png" />
    <link rel="stylesheet" type="text/css" href="components/css/Master.css" />
    <title>صفحه اصلی</title>
</head>

<body>
    {include file="templates/header.tpl" title="Info"}
    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel"  style="margin-top: 110px!important;">
        <ol class="carousel-indicators">
            <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
            <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            {foreach from=$carousel item=item name=foo}
                {if $smarty.foreach.foo.first}
                    <div class="carousel-item active">
                        <img src="{$item[6]}" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block text-center">
                            <h4>{$item[1]}</h4>
                            <p>{$item[2]}</p>
                        </div>
                    </div>
                {else}
                    <div class="carousel-item">
                        <img src="{$item[6]}" class="d-block w-100" alt="...">
                        <div class="carousel-caption d-none d-md-block text-center">
                            <h4>{$item[1]}</h4>
                            <p>{$item[2]}</p>
                        </div>
                    </div>
                {/if}
            {/foreach}
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </a>
    </div>
    <br />
    <div class="container">
        <div
            class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">NEWS</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="news.php" class="btn btn-outline-secondary btn-sm" tabindex="-1" role="button"
                    aria-disabled="true">more</a>
            </div>
        </div>
        <div class="row">
            {foreach from=$news item=item}
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="card mb-3">
                        <img src="{$item[6]}" class="card-img-top" alt="news">
                        <div class="card-img-overlay">
                            <a href="news.php" class="card-text btn btn-primary btn-sm" tabindex="-1" role="button">
                                more
                            </a>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{$item[1]}</h5>
                            <p class="card-text">{$item[2]}</p>
                            <p class="card-text">
                                <small class="text-muted">{$item[5]}</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            {/foreach}
        </div>
    </div>
    {include file="templates/footer.tpl" title="Info"}
</body>

</html>