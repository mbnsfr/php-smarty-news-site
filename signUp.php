<?php
session_start();
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 0);

//Includes necessary files
include('vendor/smarty/smarty/libs/Smarty.class.php');
include('components/php/db_connect.php');
include('components/php/common.php');


// create object
$smarty = new Smarty;

$smarty->assign('Iran_province', $Iran_province);

// display it
$smarty->display('templates/signUp.tpl');
